#include <DynamixelShield.h>
#include <SR04.h>
//----------------------------------------------------------------------------------------------------------------------------------------------------------
SR04 US_links = SR04(47, 46);  //SR04(Echo-PIN,Trigger-PIN)
int links_dist;

SR04 US_front = SR04(45, 44);
int front_dist;

SR04 US_rechts = SR04(31, 30);
int rechts_dist;
//----------------------------------------------------------------------------------------------------------------------------------------------------------
#if defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MEGA2560)
#include <SoftwareSerial.h>
SoftwareSerial soft_serial(7, 8);  // DYNAMIXELShield UART RX/TX
#define DEBUG_SERIAL soft_serial
#elif defined(ARDUINO_SAM_DUE) || defined(ARDUINO_SAM_ZERO)
#define DEBUG_SERIAL SerialUSB
#else
#define DEBUG_SERIAL Serial
#endif
//----------------------------------------------------------------------------------------------------------------------------------------------------------
const uint8_t Motor_links = 2;
const uint8_t Motor_rechts = 1;

const float DXL_PROTOCOL_VERSION = 1.0;

DynamixelShield dxl;

using namespace ControlTableItem;
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------
void setup() {
  // For Uno, Nano, Mini, and Mega, use UART port of DYNAMIXEL Shield to debug.
  DEBUG_SERIAL.begin(19200);
  // Set Port baudrate to 1000000bps. This has to match with DYNAMIXEL baudrate.
  dxl.begin(1000000);
  // Set Port Protocol Version. This has to match with DYNAMIXEL protocol version.
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);
  // Get DYNAMIXEL information
  dxl.ping(Motor_links);
  dxl.ping(Motor_rechts);
  // Turn off torque when configuring items in EEPROM area
  dxl.torqueOff(Motor_links);
  dxl.torqueOff(Motor_rechts);
  dxl.setOperatingMode(Motor_links, OP_VELOCITY);
  dxl.setOperatingMode(Motor_rechts, OP_VELOCITY);
  dxl.torqueOn(Motor_links);
  dxl.torqueOn(Motor_rechts);
}
//----------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------

float stop = -100;
float msl = -0.0;   //max speed links
float msr = 100.0;  //max speed rechts


bool linksrum = false;
bool rechtsrum = false;
bool start = true;
bool start2 = false;
bool drehenR = false;
bool drehenL = false;

//----------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------
void loop() {
  front_dist = US_front.Distance();  // Führe eine Entfernungsmessung durch (in Zentimetern)
  links_dist = US_links.Distance();
  rechts_dist = US_rechts.Distance();

  //----------------------------------------------------------------------------------------------------------------------------------------------------------
  if (start == true) {  //Erste Fahrweise wird nur einmal zu Beginn ausgeführt
    dxl.setGoalVelocity(Motor_links, msl - 60, UNIT_PERCENT);
    dxl.setGoalVelocity(Motor_rechts, msr - 60, UNIT_PERCENT);
    delay(1000);
    start2 = true;
    start = false;
  } else if (start == false) {
    dxl.setGoalVelocity(Motor_links, msl - 100, UNIT_PERCENT);
    dxl.setGoalVelocity(Motor_rechts, msr - 99, UNIT_PERCENT);
  }
  if (start2 == true) {  //Zweite Anweisung wird nur einmal zu Beginn ausgeführt (Auslesen der Sensoren -> Ergebnis: rechtsrum/linksrum fahren)
    if (links_dist > rechts_dist) {
      rechtsrum = true;
    }
    if (links_dist < rechts_dist) {
      linksrum = true;
    }
    start2 = false;
  }
  //----------------------------------------------------------------------------------------------------------------------------------------------------------
  if ((start2 == false) && (start == false)) {
    if (rechtsrum == true) {
      if (rechts_dist >= 12.75) {  //lenke nach rechts
        dxl.setGoalVelocity(Motor_links, msl - 40, UNIT_PERCENT);
        dxl.setGoalVelocity(Motor_rechts, msr - 60, UNIT_PERCENT);
      } else if (rechts_dist < 12.75) {  //lenke nach links
        dxl.setGoalVelocity(Motor_links, msl - 60, UNIT_PERCENT);
        dxl.setGoalVelocity(Motor_rechts, msr - 38, UNIT_PERCENT);
      }
    }
    if (linksrum == true) {
      if (links_dist >= 12.75) {  //lenke nach links
        dxl.setGoalVelocity(Motor_rechts, msr - 38, UNIT_PERCENT);
        dxl.setGoalVelocity(Motor_links, msl - 60, UNIT_PERCENT);
      } else if (links_dist < 12.75) {  //lenke nach rechts
        dxl.setGoalVelocity(Motor_rechts, msr - 60, UNIT_PERCENT);
        dxl.setGoalVelocity(Motor_links, msl - 40, UNIT_PERCENT);
      }
    }
    if ((front_dist <= 30) && (rechtsrum == true)) {
      drehenR = true;
    }
    if (drehenR == true) {
      dxl.setGoalVelocity(Motor_rechts, msl - 10, UNIT_PERCENT);
      dxl.setGoalVelocity(Motor_links, msr - 10, UNIT_PERCENT);
      delay(250);
      dxl.setGoalVelocity(Motor_rechts, msl - 25, UNIT_PERCENT);
      dxl.setGoalVelocity(Motor_links, msl - 25, UNIT_PERCENT);
      delay(150);
      drehenR = false;
    }
    if ((front_dist <= 20) && (linksrum == true)) {
      drehenL = true;
    }
    if (drehenL == true) {
      dxl.setGoalVelocity(Motor_rechts, msl - 10, UNIT_PERCENT);
      dxl.setGoalVelocity(Motor_links, msr - 10, UNIT_PERCENT);
      delay(150);
      dxl.setGoalVelocity(Motor_rechts, msl - 25, UNIT_PERCENT);
      dxl.setGoalVelocity(Motor_links, msl - 25, UNIT_PERCENT);
      delay(250);
      drehenL = false;
    }
  }
}
// Bot Breite = 23,5cm
// Bot Länge = 24,5cm
// Bot Breite von US_Sensoren = 14,5cm
