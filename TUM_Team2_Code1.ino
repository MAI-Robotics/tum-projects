
#include <DynamixelShield.h>
#include <SR04.h>


SR04 US_links = SR04(43, 42);  //SR04(Echo-PIN,Trigger-PIN)
int links_dist;

SR04 US_front = SR04(45, 44);
int front_dist;

SR04 US_rechts = SR04(47, 46);
int rechts_dist;

#if defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_MEGA2560)
#include <SoftwareSerial.h>
SoftwareSerial soft_serial(7, 8);  // DYNAMIXELShield UART RX/TX
#define DEBUG_SERIAL soft_serial
#elif defined(ARDUINO_SAM_DUE) || defined(ARDUINO_SAM_ZERO)
#define DEBUG_SERIAL SerialUSB
#else
#define DEBUG_SERIAL Serial
#endif

const uint8_t Motor_links = 2;
const uint8_t Motor_rechts = 1;

const float DXL_PROTOCOL_VERSION = 1.0;

DynamixelShield dxl;

//This namespace is required to use Control table item names
using namespace ControlTableItem;




void setup() {

  // For Uno, Nano, Mini, and Mega, use UART port of DYNAMIXEL Shield to debug.
  DEBUG_SERIAL.begin(19200);

  // Set Port baudrate to 1000000bps. This has to match with DYNAMIXEL baudrate.
  dxl.begin(1000000);
  // Set Port Protocol Version. This has to match with DYNAMIXEL protocol version.
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);
  // Get DYNAMIXEL information
  dxl.ping(Motor_links);
  dxl.ping(Motor_rechts);

  // Turn off torque when configuring items in EEPROM area
  dxl.torqueOff(Motor_links);
  dxl.torqueOff(Motor_rechts);
  dxl.setOperatingMode(Motor_links, OP_VELOCITY);
  dxl.setOperatingMode(Motor_rechts, OP_VELOCITY);
  dxl.torqueOn(Motor_links);
  dxl.torqueOn(Motor_rechts);
}




bool rueck = 0;

int diff_li_re;

const int max_li_vor = 1023 ;
const int max_re_vor = 2047 ;
const int max_li_rueck = 2047-200;
const int max_re_rueck = 1023-200;
const int min_li_vor = 0;
const int min_re_vor = 1024;
const int min_li_rueck = 1024;
const int min_re_rueck = 0;
int speed_links_vor ;
int speed_rechts_vor ;
int speed_links_rueck;
int speed_rechts_rueck;
bool lt_rechts;  //letzte Drehung war nach rechts
bool lt_links;   //letzte Drehung war nach links


void loop() {


  front_dist = US_front.Distance();
  links_dist = US_links.Distance();
  rechts_dist = US_rechts.Distance();


  constrain(links_dist, 2, 250);  //begrenze Wertebereich links auf 2-250cm
  constrain(rechts_dist, 2, 250); //begrenze Wertebereich rechts auf 2-250cm
  int maped_dist_links = map(links_dist, 2, 250, 1323, 1747);
  int maped_dist_rechts = map(rechts_dist, 2, 250, 300, 723);


  diff_li_re = abs((links_dist - rechts_dist));


  /*----------------------------------------------------------------------------------------------------------------------------------
  dxl.setGoalVelocity(Motor_links, min_li_vor + 700);   //Motor1 Rückwärts: 1024-2047 Vorwärts:0-1023
  dxl.setGoalVelocity(Motor_rechts, min_re_vor + 700);  //Motor2 Rückwärts: 0-1023 Vorwärts: 1024-2047

  //----------------------------------------------------------------------------------------------------------------------------------*/


  if ((links_dist < rechts_dist)  & !rueck) {  //fahr nach rechts
    lt_rechts = 1;
    lt_links = 0;

    speed_links_vor = maped_dist_rechts;
    speed_rechts_vor = maped_dist_links;

    dxl.setGoalVelocity(Motor_links, speed_links_vor);    //Motor1 Rückwärts: 1024-2047 Vorwärts:0-1023
    dxl.setGoalVelocity(Motor_rechts, speed_rechts_vor);  //Motor2 Rückwärts: 0-1023 Vorwärts: 1024-2047
  }

  //----------------------------------------------------------------------------------------------------------------------------------

  if ((rechts_dist < links_dist)  & !rueck) {  //fahr nach links
    lt_links = 1;
    lt_rechts = 0;

    speed_rechts_vor = maped_dist_links;
    speed_links_vor = maped_dist_rechts;


    dxl.setGoalVelocity(Motor_links, speed_links_vor);    //Motor1 Rückwärts: 1024-2047 Vorwärts:0-1023
    dxl.setGoalVelocity(Motor_rechts, speed_rechts_vor);  //Motor2 Rückwärts: 0-1023 Vorwärts: 1024-2047
  }


  //----------------------------------------------------------------------------------------------------------------------------------

  if (front_dist <= 10) {  //zurück
    rueck = true;
    if (lt_links) {
      dxl.setGoalVelocity(Motor_links, max_li_rueck - 500);  //Motor1 Rückwärts: 1024-2047 Vorwärts:0-1023
      dxl.setGoalVelocity(Motor_rechts, max_re_rueck);       //Motor2 Rückwärts: 0-1023 Vorwärts: 1024-2047
    }
    if (lt_rechts) {
      dxl.setGoalVelocity(Motor_links, max_li_rueck);         //Motor1 Rückwärts: 1024-2047 Vorwärts:0-1023
      dxl.setGoalVelocity(Motor_rechts, max_re_rueck - 500);  //Motor2 Rückwärts: 0-1023 Vorwärts: 1024-2047
    }
  } else {
    rueck = false;
  }
}

//Motor1: -100% = Stop | 0% oder-0% = max Speed Vorwärts  | 100%= max Speed Rückwärts
//Motor2: -100% = Stop | 100% = max Speed Vorwärts | 0% oder -0% = max Speed Rückwärts